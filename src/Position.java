import java.util.Objects;

/** Définir une position. */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " +
		// this);
	}

	public Position() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}

	public boolean equals(Object obj) {
		if (obj instanceof Position) {
			Position p = ((Position) obj);
			if (this.x == p.x && this.y == p.y) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	// pour sommeParPosition
	
	public int hashCode() {
		return Objects.hash(x, y);
	}
}

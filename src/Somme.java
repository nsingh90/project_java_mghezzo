/**
  * Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {
	private double somme = 0;
	
	
	private void Somme(String nomLot) {
		
	}


	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}


	@Override
	public double somme() {
		// TODO Auto-generated method stub
		return somme;
	}
	
	public void traiter(Position position, double valeur) {
		somme+=valeur;
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}


}
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



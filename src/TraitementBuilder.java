import java.lang.reflect.*;
import java.util.*;

/**
 * TraitementBuilder
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class TraitementBuilder {

	/**
	 * Retourne un objet de type Class correspondant au nom en paramètre. Exemples :
	 * - int donne int.class - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		if (nomType.contentEquals("int")) {
			return Integer.TYPE;
		} else if (nomType.contentEquals("double")) {
			return Double.TYPE;
		} else if (nomType.contentEquals("String") || nomType.contentEquals("java.lang.String")) {
			return String.class;

		} else {
			return Class.forName(nomType);
		}
	}

	/**
	 * Crée l'objet java qui correspond au type formel(type de param) en exploitant
	 * le « mot » suviant du scanner. Exemple : si formel est int.class, le mot
	 * suivant doit être un entier et le résulat est l'entier correspondant. Ici, on
	 * peut se limiter aux types utlisés dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		if (formel.equals(Integer.TYPE)) {
			return Integer.parseInt(in.next());
		} else if (formel.equals(Double.TYPE)) {
			return Double.parseDouble(in.next());
		} else if (formel.equals(String.class)) {
			return in.next();
		} else {
			return null; // TODO à remplacer
		}

	}

	/**
	 * Définition de la signature, les paramètres formels, mais aussi les paramètres
	 * formels.
	 */
	static class Signature {
		Class<?>[] formels; // type de param
		Object[] effectifs; // valeur de param

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/**
	 * Analyser une signature pour retrouver les paramètres formels et les
	 * paramètres effectifs. Exemple « 3 double 0.0 java.lang.String xyz int -5 »
	 * donne - [double.class, String.class, int.class] pour les paramètres effectifs
	 * et - [0.0, "xyz", -5] pour les paramètres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		int tailleTableau = in.nextInt();
		Class<?>[] formels = new Class<?>[tailleTableau];
		Object[] effectifs = new Object[tailleTableau];
		if (tailleTableau == 0) {
			return new Signature(formels, effectifs);
		} else {
			int formelCompteur = 0;
			int effectifCompteur = 0;
			while (tailleTableau-- > 0) {
				formels[formelCompteur] = analyserType(in.next());
				effectifs[effectifCompteur] = decoderEffectif(formels[formelCompteur], in);
				formelCompteur++;
				effectifCompteur++;
			}

			return new Signature(formels, effectifs);
		}

	}

	/**
	 * Analyser la création d'un objet. Exemple : « Normaliseur 2 double 0.0 double
	 * 100.0 » consiste à charger la classe Normaliseur, trouver le constructeur qui
	 * prend 2 double, et l'appeler en lui fournissant 0.0 et 100.0 comme paramètres
	 * effectifs.
	 */
	Object analyserCreation(Scanner in) throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException, InstantiationException {
		Class<?> classe = analyserType(in.next());
		//creer un insrance pour recuperer le type et le param@
		Signature signature = analyserSignature(in);
		//pr definir le contructeur on  utilisant les param@ recuperer
		Constructor<?> constructor = classe.getConstructor(signature.formels);
		// creer un intance apres pouvoir creer le constructeur 
		Object obj = constructor.newInstance(signature.effectifs);
		return obj;
	}

	/**
	 * Analyser un traitement. Exemples : - « Somme 0 0 » - « SupprimerPlusGrand 1
	 * double 99.99 0 » - « Somme 0 1 Max 0 0 » - « Somme 0 2 Max 0 0
	 * SupprimerPlusGrand 1 double 20.0 0 » - « Somme 0 2 Max 0 0 SupprimerPlusGrand
	 * 1 double 20.0 1 Positions 0 0 »
	 * 
	 * @param in  le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env) throws ClassNotFoundException,
			InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {
		Traitement traite = (Traitement) analyserCreation(in);
		int nombreTraiteSuivant = in.nextInt();
		while (nombreTraiteSuivant-- > 0) {
			traite.ajouterSuivants(analyserTraitement(in, env));

		}
		return traite;
	}

	/**
	 * Analyser un traitement.
	 * 
	 * @param in  le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env) {
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, " + "voir la cause ci-dessous", e);
		}
	}

}

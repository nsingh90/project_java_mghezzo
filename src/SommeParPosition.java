import java.util.*;

/**
 * SommeParPosition
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class SommeParPosition extends Traitement {

	// TODO à faire...

	HashMap<Position, Double> listPositionValeur = new HashMap<Position, Double>();

	public void traiter(Position position, double valeur) {
		if (listPositionValeur.containsKey(position)) {
			listPositionValeur.replace(position, listPositionValeur.get(position) + valeur);
		} else {
			listPositionValeur.put(position, valeur);
		}
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	protected void gererFinLotLocal(String nomLot) {
		System.out.println("SommeParPosition " + nomLot + " : ");
		listPositionValeur.entrySet().forEach(entry -> {
			System.out.println("\t-" + entry.getKey() + " -> " + entry.getValue());
		});
		System.out.println("Fin " + this.getClass().getName());

	}
}

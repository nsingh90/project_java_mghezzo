/**
 * SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class SupprimerPlusGrand extends Traitement {

	// TODO à faire...

	private double seuil;

	public SupprimerPlusGrand(double seuil) {
		this.seuil = seuil;
	}

	public void traiter(Position position, double valeur) {
		if (valeur <= this.seuil) {
			for (Traitement suivant : this.suivants) {
				suivant.traiter(position, valeur);
			}

		}

	}

	protected String toStringComplement() {
		return ("> " + this.seuil);
	}

}

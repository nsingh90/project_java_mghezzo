import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Donnees enregistre toutes les données reçues, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Donnees extends Traitement {

	// TODO à faire...

	private ArrayList<Position> listDePositions = new ArrayList<Position>();
	private ArrayList<Double> listDeValeurs = new ArrayList<Double>();
	Iterable<SimpleImmutableEntry<Position, Double>> source = new ArrayList<AbstractMap.SimpleImmutableEntry<Position, Double>>();
	
	public void traiter(Position position, double valeur) {
		listDePositions.add(position);
		listDeValeurs.add(valeur);
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	public void importDonneesTypes1(String nomFichier) {
		try (BufferedReader fichier = new BufferedReader(new FileReader(nomFichier))) {
			String line = null;
			while ((line = fichier.readLine()) != null) {
				String[] mot = line.split("\\s");
				this.listDeValeurs.add(Double.parseDouble(mot[3]));
				int x = Integer.parseInt(mot[0]);
				int y = Integer.parseInt(mot[1]);
				Position p = new Position(x, y);
				listDePositions.add(p);
				SimpleImmutableEntry<Position, Double> element = new SimpleImmutableEntry<Position, Double>(p,
						Double.parseDouble(mot[3]));

				((ArrayList<SimpleImmutableEntry<Position, Double>>) source).add(element);
			}

		} catch (FileNotFoundException e) {
			System.out.println("fichier introuvalbe !");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			System.out.println("impossible d'ouvrir le fichier ");
			e.printStackTrace();
			System.exit(0);
		}

	}

	public void importDonneesTypes2(String nomFichier) {
		try (BufferedReader fichier = new BufferedReader(new FileReader(nomFichier))) {
			String line = null;
			while ((line = fichier.readLine()) != null) {
				String[] mot = line.split("\\s");
				this.listDeValeurs.add(Double.parseDouble(mot[4]));
				int x = Integer.parseInt(mot[1]);
				int y = Integer.parseInt(mot[2]);
				Position p = new Position(x, y);
				listDePositions.add(p);
				SimpleImmutableEntry<Position, Double> element = new SimpleImmutableEntry<Position, Double>(p,
						Double.parseDouble(mot[4]));
				((ArrayList<SimpleImmutableEntry<Position, Double>>) source).add(element);

			}

		} catch (FileNotFoundException e) {
			System.out.println("fichier introuvable !");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			System.out.println("impossible d'ouvrir le fichier ");
			e.printStackTrace();
			System.exit(0);
		}
	}

	
	public void importFichierXml(String nomFichier) {
		try {
			File fichier = new File(nomFichier);
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			org.w3c.dom.Document document = documentBuilder.parse(fichier);
			if (document.hasChildNodes()) {
				recupererElement(document.getChildNodes());
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void recupererElement(NodeList childNodes) {
		// TODO Auto-generated method stub
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node element = childNodes.item(i);
			if (element.getNodeType() == Node.ELEMENT_NODE) {
				double valeur = 0;
				Position position = new Position();
				if (element.getNodeName() == "valeur") {
					valeur = Double.parseDouble(element.getTextContent());
				}
				if (element.getNodeName() == "x") {
					position.x = Integer.parseInt(element.getTextContent());
				}
				if (element.hasAttributes()) {
					NamedNodeMap attribut = element.getAttributes();
					for (int cpt = 0; cpt < attribut.getLength(); cpt++) {
						Node attNode = attribut.item(cpt);
						if (attNode.getNodeName() == "y") {
							position.y = Integer.parseInt(attNode.getTextContent());
						}
					}
				}
				SimpleImmutableEntry<Position, Double> elem = new SimpleImmutableEntry<Position, Double>(position,
						valeur);
				((ArrayList<SimpleImmutableEntry<Position, Double>>) source).add(elem);
			}
		}

	}

}

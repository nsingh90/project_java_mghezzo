/**
  * SupprimerPlusPetit supprime les valeurs plus petites qu'un seuil.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusPetit extends Traitement {

	// TODO à faire...
	
	private double seuil;

	public SupprimerPlusPetit(double seuil) {
		this.seuil = seuil;
	}

	public void traiter(Position position, double valeur) {
		if (valeur <= this.seuil) {
			for (Traitement suivant : this.suivants) {
				suivant.traiter(position, valeur);
			}

		}

	}

	protected String toStringComplement() {
		return ("< " + this.seuil);
	}

}

/**
 * Multiplicateur transmet la valeur multipliée par un facteur.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Multiplicateur extends Traitement {

	// TODO à faire...
	private double facteur;

	public Multiplicateur(double facteur) {
		this.facteur = facteur;
	}

	public void traiter(Position position, double valeur) {
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur * facteur);
		}
	}

	protected String toStringComplement() {
		return " * " + facteur;

	}
}


// Exam 1
import javax.crypto.Mac;

/**
 * Normaliseur normalise les données d'un lot en utilisant une transformation
 * affine.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Normaliseur extends Traitement {

	// TODO à faire...
	private double debut;
	private double fin;
	private Max max;

	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
		max = new Max();
	}

	// a×x+b avec a= (M−m)/(f−d) et b = d−a×m.

	public void traiter(Position position, double valeur) {
		max.setMax(valeur);
		max.setMin(valeur);
		double a = (max.getMax() - max.getMin()) / (this.fin - this.debut);
		double b = this.debut - a * max.getMin();

		valeur = a * valeur + b;

		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}
}

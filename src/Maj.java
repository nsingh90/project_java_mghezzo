import java.util.ArrayList;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	// TODO à faire...
	
	private ArrayList<Position> listDesPositions = new ArrayList<Position>();
	
	
	public void traiter(Position position, double valeur) {
		if(!listDesPositions.contains(position)) {
			listDesPositions.add(position);
		}
		
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

}
